
public interface ICarBuilder {
	void setColour(String colour);
	void setWheels(int count);
	Car getResult();
}
