
public interface Observer {
	void update(Observable ob);
}
